#!/usr/local/bin/python
import json
import boto3
from collections import defaultdict
from botocore.exceptions import ClientError
from datetime import datetime
import ast

## Global variables

# Connect to EC2
ec2 = boto3.resource('ec2')

instances_to_stop = []

# Retrieves all regions/endpoints that work with EC2
def findregions():
    regions = []
    ec2 = boto3.client('ec2')
    response = ec2.describe_regions()
    for region in response['Regions']:
      regions.append(region['RegionName'])
    return regions


# Searches through running instances to see if there are any that are too big

def searchinst(theregion):
    global instances_to_stop
    localinstance = 0
    # Get information for all running instances
    ec2 = boto3.resource('ec2', region_name=theregion)
    running_instances = ec2.instances.filter(Filters=[{
        'Name': 'instance-state-name',
        'Values': ['running']}])


    # Searches for the types and prints a warning error for instances that are not in the allowed types

    allowedtypes = ["t2.nano", "t2.micro", "t2.medium", "t2.large", "t2.xlarge", "t2.2xlarge", "t2.small"]

    for instance in running_instances:
        if instance.instance_type in allowedtypes:
            pass
        else:
            print ("Warning! Instance of type "+instance.instance_type+" is running with id "+instance.id+" in "+theregion)
    # Adding instance ids of big instances to an array so it can be used later, e.g. in notifications
            instances_to_stop.append(instance.id+":"+theregion)
            localinstance = 1

    if localinstance == 0:
        print ("Everything is A-okay in "+theregion+". All running instances are of allowed types. No instances will be stopped")



# Start instances (don't actually use this but it's there)

def startinst(info):
    vmdetails=info.split(":")
    id=vmdetails[0]
    theregion=vmdetails[1]
    ec2 = boto3.client('ec2', region_name=theregion)
    ec2.start_instances(InstanceIds=[id])
    print ("started your instances: " + str(id))

# Stop instances

def stopinst(info):
    vmdetails=info.split(":")
    id=vmdetails[0]
    theregion=vmdetails[1]
    ec2 = boto3.client('ec2', region_name=theregion)
    ec2.stop_instances(InstanceIds=[id])


## This starts/stops a specific instance depending on whether it's stopped/running

# def lambda_handler(event, context):
#     searchinst()
#     instances = ['i-05bd839bd2097f40e']
#     for instanceid in instances:
#         instanceObj = ec2.Instance(instanceid)
#         if instanceObj.state["Name"] == "running":
#             stopinst(instanceid)
#         if instanceObj.state["Name"] == "stopped":
#             startinst(instanceid)
#         else:
#             print ("Instance is in another state which I'm going to ignore for now")
#
#         instanceObj=None


# Send an email

def emailme(instances_to_stop):

    # This address must be verified with Amazon SES!
    SENDER = "Katie's instance alert <catherine@automationlogic.com>"

    # This also needs to be verified!
    RECIPIENT = "catherine@automationlogic.com"

    AWS_REGION = "us-east-1"

    # The subject line for the email.
    SUBJECT = "One or more instances have been stopped!"

    # The email body for recipients with non-HTML email clients.
    BODY_TEXT = "Amazon SES Test (Python)\r\n"+",".join(instances_to_stop)+"This email was sent with Amazon SES using the AWS SDK for Python (Boto)."


    # The HTML body of the email.
    BODY_HTML = """<html>
    <head></head>
    <body>
      <h2>An instance has been stopped!</h2>
      <h3>The following instances have been stopped, because their size was not in the allowed range: </h3>
    """
    BODY_HTML = BODY_HTML+"<br>".join(instances_to_stop)
    BODY_HTML = BODY_HTML+"""
      <p>The allowed types of ec2 instance are: t2.nano, t2.micro, t2.small, t2.medium, t2.large, t2.xlarge, t2.2xlarge</p>
      <p>This email was sent with
        <a href='https://aws.amazon.com/ses/'>Amazon SES</a> using the
        <a href='https://aws.amazon.com/sdk-for-python/'>
          AWS SDK for Python (Boto)</a>.</p>
    </body>
    </html>
                """

    # The character encoding for the email.
    CHARSET = "UTF-8"

    # Create a new SES resource and specify a region.
    client = boto3.client('ses', region_name=AWS_REGION)

    # Try to send the email.
    try:
        # Provide the contents of the email.
        response = client.send_email(
            Destination={
                'ToAddresses': [
                    RECIPIENT,
                ],
            },
            Message={
                'Body': {
                    'Html': {
                        'Charset': CHARSET,
                        'Data': BODY_HTML,
                    },
                    'Text': {
                        'Charset': CHARSET,
                        'Data': BODY_TEXT,
                    },
                },
                'Subject': {
                    'Charset': CHARSET,
                    'Data': SUBJECT,
                },
            },
            Source=SENDER,
        )

    # Display an error if something goes wrong.
    except ClientError as e:
        print(e.response['Error']['Message'])
    else:
        print("Email sent! Message ID:"),
        print(response['MessageId'])


# The main lambda handler which calls upon each function

def lambda_handler(event, context):
    global instances_to_stop
    allregions=findregions()
    for aregion in allregions:
        searchinst(aregion)
    for instanceid in instances_to_stop:
        stopinst(instanceid)
    print ("The following instances have been stopped:")
    print list(set(instances_to_stop))

    if not instances_to_stop:
        pass
    else:
        emailme(list(set(instances_to_stop)))

    instances_to_stop=[]
