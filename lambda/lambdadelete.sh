#!/bin/bash

## SCRIPT TO DELETE IAM ROLE, POLICY AND LAMBDA FUNCTION ##

export AWS_DEFAULT_REGION=us-east-1

# this grabs the variables created in the create script from the temp file so I can use them here
prefix=$(cat /tmp/prefix.$PPID)
policyarn=$(cat /tmp/policyarn.$PPID)


# remove targets 1
aws events remove-targets --rule $prefix-rule1 --ids 1

# delete rule 1
aws events delete-rule --name $prefix-rule1
echo "Rule 1 deleted"

# remove targets 2
aws events remove-targets --rule $prefix-rule2 --ids 1

# delete rule 2
aws events delete-rule --name $prefix-rule2
echo "Rule 2 deleted"


# delete lambda function
aws lambda delete-function --function-name $prefix-function
echo "Lambda function deleted"

# detach policy from role
aws iam detach-role-policy --role-name $prefix-rol --policy-arn $policyarn

# delete role
aws iam delete-role --role-name $prefix-rol
echo "Role deleted"

# delete policy
aws iam delete-policy --policy-arn $policyarn
echo "Policy deleted"
