#!/bin/bash

## SCRIPT TO UPDATE LAMBDA FUNCTION ##

export AWS_DEFAULT_REGION=us-east-1

prefix=$(cat /tmp/prefix.$PPID)

# zip up python file
zip -9 katie.zip main.py

# update function
aws lambda update-function-code --function-name $prefix-function  --zip-file fileb://katie.zip
