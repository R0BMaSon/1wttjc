#!/bin/bash

## SCRIPT TO CREATE IAM ROLE, POLICY AND LAMBDA FUNCTION ##

# variables to export to use in other scripts:
export prefix=bork
printf %s "$prefix" > /tmp/prefix.$PPID

export AWS_DEFAULT_REGION=us-east-1
# North Virginia, need to create the function here to allow SES

# create IAM policy
export policyarn=`aws iam create-policy --policy-name $prefix-pol --policy-document file://ec2policy.json --output text | awk '{ print $2 }'`
printf %s "$policyarn" > /tmp/policyarn.$PPID
echo "Created IAM policy $prefix-pol with arn $policyarn"

# create IAM role
rolearn=`aws iam create-role --role-name $prefix-rol --assume-role-policy-document file://ec2role.json --output text | awk 'NR==1{print $2}'`
echo "Created IAM role $prefix-rol with arn $rolearn"

# attach policy to role
aws iam attach-role-policy --policy-arn $policyarn --role-name $prefix-rol
echo "Attached $prefix-pol to $prefix-rol"

# needed a little sleep as function wasn't creating straight after role creation
echo "Having a nap..."
sleep 20
# (was 10 but had to make it longer when I added the def searchinst)


# zip up python file
zip -9 katie.zip main.py

# create the function
functionarn=`aws lambda create-function --function-name $prefix-function --runtime python2.7 --role $rolearn --handler main.lambda_handler --description 'Please dont break everything' --timeout 20 --tags Name=$prefix-Lambda --zip-file fileb://katie.zip --output text | awk 'NR==1{print $7}'`
# note, print $7 as arn is 7th thing. 3rd 4th 5th 6th please don't break everything. so this depends on description of function. will do this a more clever way if I have time (I did not have time)

echo "Created lambda function $prefix-function with arn $functionarn"


# add scheduled rules to lambda function:

# make rule 1 (should execute every hour between 00:00 and 07:00 and between 20:00 and 23:00 on weekdays)
rule1arn=`aws events put-rule --name $prefix-rule1 --schedule-expression "cron(0 0-7,20-23 ? * MON-FRI *)" --output text`
echo "Made the rule to schedule the lambda function every hour midnight-7am, and 8pm-11pm, Mon-Fri"

# add permission to the rule for the lambda function

aws lambda add-permission --function-name $prefix-function --action 'lambda:InvokeFunction' --principal events.amazonaws.com --source-arn $rule1arn --statement-id 12345

# add lambda function to rule
aws events put-targets --rule $prefix-rule1 --targets "Id"="1","Arn"="$functionarn"
echo "Added the rule to the lambda function"


# make rule 2 (should execute every hour on Sat and Sun)
rule2arn=`aws events put-rule --name $prefix-rule2 --schedule-expression "cron(0 * ? * SAT,SUN *)" --output text`
echo "Made the rule to schedule the lambda function every hour on weekends"

# add permission to the rule for the lambda function

aws lambda add-permission --function-name $prefix-function --action 'lambda:InvokeFunction' --principal events.amazonaws.com --source-arn $rule2arn --statement-id 1234567

# add lambda function to rule
aws events put-targets --rule $prefix-rule2 --targets "Id"="1","Arn"="$functionarn"
echo "Added the rule to the lambda function"
