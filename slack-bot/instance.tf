
resource "aws_instance" "jenkins" {
    ami = "${var.image_id}"
    instance_type = "${var.instance_type}"
    key_name      = "${var.key_name}"
    associate_public_ip_address = true
    tags {
        Name = "${var.environment}-jenkins"
    }

    connection {
      user         = "ec2-user"
      private_key  = "${file("~/.aws/rahmatkey.pem")}"
    }

    provisioner "local-exec" {
      command =  "echo \"[jenkins]\" > hosts"
    }

    provisioner "local-exec" {
      command =  "echo \"${format( aws_instance.jenkins.public_ip)}\" >> hosts&"
    }

    # provisioner "local-exec" {
    #     command     = "ansible-playbook -i hosts site.yml"
    # }

#Monitor system disk usage
user_data = <<HEREDOC


#!/bin/sh
yum install -y https://s3-us-west-2.amazonaws.com/grafana-releases/release/grafana-5.0.3-1.x86_64.rpm
service grafana-server start
/sbin/chkconfig --add grafana-server


HEREDOC

}
