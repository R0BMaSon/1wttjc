#!/bin/bash

# This file is run using the following command: source mkenv.sh

# TF_VAR_ bit is terraform style, it takes the bit after this


TF_VAR_access_key=$(grep -i 'aws_access_key' ~/.aws/credentials | awk '{print $NF}')

TF_VAR_secret_key=$(grep -i 'aws_secret_access_key' ~/.aws/credentials | awk '{print $NF}')



TF_VAR_database_password=interestingsecret


export TF_VAR_database_password TF_VAR_access_key TF_VAR_secret_key

# move into .profile in home directory so don't have to always run source mkenv.sh
