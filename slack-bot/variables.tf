variable "access_key" {}
variable "secret_key" {}

variable "region" {
  default = "eu-west-2"
}

variable "prefix" {
  default = "jbrv"
}

# variables
variable "vpc_cidr" {
  description = "The CIDR block of the vpc"
  default = "10.0.0.0/16"
}

variable "public_subnets_cidr" {
  type        = "list"
  description = "The CIDR block for the public subnet"
  default = [
    "10.0.101.0/24",
    "10.0.103.0/24"
  ]
}

variable "private_subnets_cidr" {
  type        = "list"
  description = "The CIDR block for the private subnet"
  default = [
    "10.0.202.0/24",
    "10.0.201.0/24"
  ]
}

variable "environment" {
  description = "The environment"
  default = "rahmatpract"
}

variable "availability_zones" {
  type        = "list"
  description = "The az that the resources will be launched"
  default = [
    "eu-west-2a",
    "eu-west-2b"
  ]
}

variable "key_name" {
  description = "The public key for the bastion host"
  default = "rahmatkey"
}

variable "instance_type" {
  default = "t2.micro"
}

variable "image_id" {
  default = "ami-0274e11dced17bb5b"
}

variable "repository_name" {
  default = "httpd"
}

variable "secret_key_base" {
  default = "thisismysecretkey"
}

# variable "database_username" {
#   default = "admin"
# }
#
# variable "database_password" {
#   default = "password123"
# }

variable "allocated_storage" {
  default = "5"
}

variable "execution_role_arn" {
  default = "arn:aws:iam::109964479621:role/ecs_task_execution_role"
}

variable "database_name" {
  default = "a4"
}

variable "sfn_arn"         { }
variable "sns_topic_arn"        { }
