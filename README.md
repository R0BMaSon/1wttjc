# Final Project README :books: #

## Rahmat, Bobby and Katie ##


* Download the git repo from bitbucket

This code has three separate parts:

### Lambda ###

This code creates an IAM policy, role and a lambda function with scheduled events. When it runs it will stop any instances across all regions that aren't of allowed types. It will then send an email notifying the recipient of which instances have been stopped. The scheduled rules attached to the function allow it to run every hour of non-working hours.
Note: the lambda function is created in us-east-1, to allow SES notifications.

Changes to make to *lambdacreate.sh*:

* Prefix (line 6) - this will give all your resources the same tag name, so you can change this to whatever you like.
* The cron expression for the scheduled events (line 44 and 57) - The times can be changed if necessary for when you want the function to run. (It's currently set for every hour 8pm-7am Mon-Fri and every hour on Sat-Sun.)

Changes to make to *main.py*:

* Allowed types (line 41) - everything in this list is allowed, if an instance is running of any other type then it will be stopped when the lambda function runs.
* Email addresses (line 100 and 103) - The email address you wish to send the notification from and to. Both emails must first be [verified with aws](https://docs.aws.amazon.com/ses/latest/DeveloperGuide/verify-email-addresses-procedure.html).

Now we're ready to build:

  1. cd into the lambda folder
  2. Run **./lambdacreate** in the terminal
  3. View the lambda function that has been created here [Lambda function](https://console.aws.amazon.com/lambda/home?region=us-east-1#/functions)
  (You will need to log in to aws)
  4. Click on the function
  5. Click on **test** to configure a test event
  6. Give it any name, and delete everything in the code except the brackets **{}**
  7. Click **create** and then **test**
  8. Now the lambda function is running!
  9. If you make changes to *main.py* after creating the function, then you can run **./lambdaupdate** to update the lambda
  10. If you want to delete the function, run **./lambdadelete** to delete the function (this also deletes the IAM policy and role)

  (Note, if you want to run the test straight from the command line, skip step 4-7 and run:

      aws lambda invoke --function-name prefix-function --region us-east-1 name.out)

### Grafana ###

This code creates an EC2 instance which is provisioned to run Grafana. When this runs it will create two dashboards which show the aws billing, and aws ec2 instance information for one of the AL amazon accounts - though this can be adapted for other companies and most information is a variable.

Changes to make to *group_vars/all*:

* You need to substitute the name of your key.pem in the first line of the file in order to create the EC2 instance

Changes to make to the *group_vars/provisioner*

* Everything below ansible_connection is fully configurable. However, for the first run of this script the admin_password and admin_user must remain as admin.

Running the playbook!

  1. cd into the grafana directory
  2. Configure the *group_vars* directory for your set up
  3. On the command line, run `ansible-playbook -i environments/prod site.yml` (This will create a Grafana server at the address (bork.grads.al-labs.co.uk:3000)
  4. log-on to the server with the admin user and password and check that the dashboards are all there!
  5. to change the password for the admin user run the command `ansible-playbook -i environments/prod site.yml --limit provisioner`
